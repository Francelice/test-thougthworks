package com.thoughtworks.trains.exceptions;

/**
 * This exception is meant to be used when there is
 * no way of processing a route calculation
 */
public class InvalidRouteException extends Exception {
}

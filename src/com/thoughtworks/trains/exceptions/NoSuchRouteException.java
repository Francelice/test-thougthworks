package com.thoughtworks.trains.exceptions;

/**
 * This exception is used when there
 * is no route betweet two Towns
 */
public class NoSuchRouteException extends Exception {
}

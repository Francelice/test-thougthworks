package com.thoughtworks.trains.exceptions;

/**
 * This exceptions is used to indicate user that no valid
 * graph could be constructed
 *
 */
public class InvalidFormatException extends Exception{

}

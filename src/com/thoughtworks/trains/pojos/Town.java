package com.thoughtworks.trains.pojos;


import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Town class represents a town in Kiwiland
 * which has at least one route on railroad
 * services
 *
 */

public class Town{


    protected String name;
    private ArrayList<Route> toThisTown;
    private ArrayList<Route> fromThisTown;


    //Constructor
    public Town(){
        this.name = new String();
        this.toThisTown = new ArrayList<>();
        this.fromThisTown = new ArrayList<>();
    }

    public Town(String name){
        this.name = name;
        this.toThisTown = new ArrayList<>();
        this.fromThisTown = new ArrayList<>();
    }


    //Getters and Setters
    public void setName(String name){this.name = name;}
    public String getName(){return this.name;}
    public ArrayList<Route> getToThisTown() {
        return toThisTown;
    }
    public void setToThisTown(ArrayList<Route> toThisTown) {
        this.toThisTown = toThisTown;
    }
    public ArrayList<Route> getFromThisTown() {
        return fromThisTown;
    }
    public void setFromThisTown(ArrayList<Route> fromThisTown) {
        this.fromThisTown = fromThisTown;
    }


    /**
     * Returns direct path from this to Town t if exists
     * @param t destination Town
     * @return
     */
    public ArrayList<Route> getRouteTo(Town t){

        ArrayList<Route> result = (ArrayList<Route>) fromThisTown.stream().filter((r) -> {
            return r.getToTown().equals(t);
        }).collect(Collectors.toList());

        if(result!=null && result.size() == 0)
            result = null;

        return result;
    }


    @Override
    public boolean equals(Object obj) {
        final Town other = (Town) obj;
        return other != null && this.name != null && this.name.equals(other.name);
    }

    @Override
    public String toString() {
        return this.name.toString() +
                " Paths to this: " + this.getToThisTown() +
                " Paths from this: "+this.getFromThisTown();
    }
}

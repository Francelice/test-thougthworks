package com.thoughtworks.trains.pojos;

/**
 * This class represents a route between two towns
 *
 */
public class Route {

    private Town fromTown;
    private Town toTown;
    private int distance; //Distance is assumed to be an integer
    private int steps;

    public String path; //debug purposes


    //Constructors
    public Route(){

        this.fromTown = new Town();
        this.toTown = new Town();
        this.distance = 0;
        this.steps = 0;
    }

    public Route(String from, String to, int distance, int steps){

        this.fromTown = new Town(from);
        this.toTown = new Town(to);
        this.distance = distance;
        this.steps = steps;
    }

    public Route(Town from, Town to, int distance, int steps){

        this.fromTown = from;
        this.toTown = to;
        this.distance = distance;
        this.steps = steps;
    }

    //Getters and setters
    public void setFromTown(Town fromTown) {
        this.fromTown = fromTown;
    }
    public void setToTown(Town toTown) {
        this.toTown = toTown;
    }
    public void setDistance(int distance) {
        this.distance = distance;
    }
    public void setSteps(int steps) {
        this.steps = steps;
    }
    public Town getFromTown() {
        return this.fromTown;
    }
    public Town getToTown() {
        return this.toTown;
    }
    public int getDistance() {
        return this.distance;
    }
    public int getSteps() {
        return this.steps;
    }


    /**
     * Given this route, appends a new route to build a new one
     * @param to
     * @return
     */
    public Route concatenate(Route to){

        int d = (this.getDistance()+to.getDistance());
        int s = (this.getSteps()+1);
        Route newRoute = new Route(this.fromTown , to.getToTown(),d, s);
        newRoute.path = this.path + to.getToTown().getName();

        return  newRoute;
    }

    @Override
    public boolean equals(Object obj) {
        final Route other = (Route) obj;
        return this.fromTown.equals(other.fromTown) &&
                this.toTown.equals(other.toTown) &&
                this.steps == other.steps &&
                this.distance == other.distance;
    }

    @Override
    public String toString() {
        return "Route[" +this.path+ "] " +
                this.fromTown.getName().toString() + " " +
                this.toTown.getName().toString() +
                " d: " + this.distance +
                " s: " + this.steps ;
    }
}

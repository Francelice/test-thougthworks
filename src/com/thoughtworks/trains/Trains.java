package com.thoughtworks.trains;

import com.thoughtworks.trains.dataProcessing.DataReader;
import com.thoughtworks.trains.dataProcessing.DataUserInputReader;
import com.thoughtworks.trains.exceptions.InvalidFormatException;
import com.thoughtworks.trains.operations.RailroadInformation;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Assumptions:
 * - Must be at least one valid route to start service
 * - In Kiwiland there is no more than one direct route between two towns
 *   and the first one indicated is the valid one
 * - All invalid routes are listed to system administrator,
 *   but we take the valid ones and work with them
 * - A route from A to A is considered invalid
 * - Distance between two towns are assumed to be integers
 */

public class Trains {


    public static void init(String arg){


        DataReader reader;


        // At the moment, we are reading data from user input
        // as well by program arguments
        // If input is passed by program argument it will be
        // set, otherwise it will be asked from console input
        reader = arg == null? new DataUserInputReader() :new DataUserInputReader(arg);

        // Here we could consider another routes input methods
        // like webservices, read from config file, etc

        //Read routes data input
        reader.readData();

        try {
            //Validate routes input
            reader.validateData();

            //Build information data structure
            RailroadInformation.initData(reader.parseData());


        }catch (InvalidFormatException e){
            Logger.getAnonymousLogger().log(Level.SEVERE,"Sorry, the input format introduced is incorrect. \nExiting service now");
        }


    }

    public static void main(String [] args){

        if(args.length > 0){
            init(args[0]);
        }else{
            init(null);
        }


    }

}

package com.thoughtworks.trains.dataProcessing;

import com.thoughtworks.trains.exceptions.InvalidFormatException;
import com.thoughtworks.trains.pojos.Route;

import java.util.ArrayList;

/**
 *
 * This interface is meant to rule all different
 * types of input our program can receive. This program is designed to receive
 * input as a program argument or from user input, but might be as well
 * a config file or a webservice consumed
 *
 */
public interface DataReader {

    public void readData();
    public ArrayList<Route> parseData();
    public void validateData() throws InvalidFormatException;

}

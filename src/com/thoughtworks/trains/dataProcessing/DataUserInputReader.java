package com.thoughtworks.trains.dataProcessing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.thoughtworks.trains.exceptions.InvalidFormatException;
import com.thoughtworks.trains.pojos.Route;

/**
 * This class takes input from user and process it
 * to transform it into Routes dataProcessing structure.
 * Implements interface DataReader as one of the options of
 * getting graph information
 */
public class DataUserInputReader implements DataReader{

    private String input;
    private ArrayList<String> validRoutes =  new ArrayList<>();
    private ArrayList<String> invalidRoutes = new ArrayList<>();

    //Constructor
    public DataUserInputReader(){}
    public DataUserInputReader(String input){
        this.input = input;
    }

    //Getters and setters
    public String getInput() {
        return input;
    }
    public void setInput(String input) {
        this.input = input;
    }


    /**
     * Read data from user if is not setted from program argument
     */
    public void readData(){

        if(this.input == null){

            System.out.println("\n");
            System.out.println("Please enter your input data. Remember format is a list of comma separated Strings");
            System.out.println("with formar XYZ where X and Y takes values of one single caracter word");
            System.out.println("and Z is an integer \n");
            System.out.println("Input Data < ");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String s = null;

            try {
                this.input = br.readLine();
            } catch (IOException e) {

            }
        }
    }


    /**
     * Data muust acomplish a format specification in roder to
     * build a valid graph. Nodes eith invalid format are ignored
     * @throws InvalidFormatException
     */
    public void validateData() throws InvalidFormatException{

        String [] routes = this.input.split("\\s*,\\s*");
        for(String route: routes){

            route = route.trim();

            if(route == null || route.isEmpty() || !route.matches("[a-zA-z][a-zA-z]\\d+")){
                invalidRoutes.add(route);
            }else{

                //We wont take routes from an to same town as valids
                if(route.substring(0,1).equalsIgnoreCase(route.substring(1,2))){
                    invalidRoutes.add(route);
                }else{
                    validRoutes.add(route);
                }
            }

        }

        //Validations
        if(invalidRoutes.size() > 0 && validRoutes.size() > 0){
            StringBuffer message = new StringBuffer();
            message.append("Some routes do not acomplished required format\n");
            message.append("They are listed below\n");

            for (String temp: invalidRoutes) {
                message.append(temp);
                message.append("\n");
            }

            Logger.getAnonymousLogger().log(Level.WARNING,  message.toString());
            
        }

        //We assume that we have a graph with at least one route
        if(validRoutes.size() == 0){
            Logger.getAnonymousLogger().log(Level.WARNING, "There are no routes that acomplished required format");
            throw new InvalidFormatException();
        }


    }

    /**
     * Convert to Business object level data
     * @return
     */
    public ArrayList<Route> parseData(){

        ArrayList<Route> routes = new ArrayList<>();
        for(String route: validRoutes){

            //Repeated values are ignored
            if(routes.indexOf(route) == -1){
                Route temp = new Route(route.substring(0,1).toUpperCase(), route.substring(1,2).toUpperCase(), Integer.parseInt(route.substring(2,route.length())),1);

                //debugging purposes
                temp.path = route.substring(0,2);
                routes.add(temp);
            }

        }

        return routes;
    }

}

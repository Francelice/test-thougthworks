package com.thoughtworks.trains.operations;

import com.thoughtworks.trains.exceptions.InvalidRouteException;
import com.thoughtworks.trains.exceptions.NoSuchRouteException;
import com.thoughtworks.trains.pojos.Route;

import java.util.*;
import com.thoughtworks.trains.pojos.Town;


/**
 *
 * This singleton is meant to hold routes information
 * and operations to answer user queries
 *
 */
public class RailroadInformation {

    private static ArrayList<Town> graph = new ArrayList<>();
    private static boolean init = false;

    private static RailroadInformation ourInstance = new RailroadInformation();

    private RailroadInformation() {}

    public static RailroadInformation getInstance() {
        return ourInstance;
    }

    public static void initData(ArrayList<Route> routes){

        //Build starting graph from user input
        if(!init){
            initGraph(routes);
            init = true;
        }

    }

    /**
     * Calculates distance of specified route only using direct routes between each point
     * @param routeRepresentation
     * @return route distance
     * @throws NoSuchRouteException when there is no direct route between some of the points
     */
    public static int routeDistance(ArrayList<Town> routeRepresentation) throws NoSuchRouteException{

        //Validate input
        if(routeRepresentation == null || routeRepresentation.size() <= 1)
            throw new NoSuchRouteException();

        int distance = 0;
        Town previous = null;
        for (Town town: routeRepresentation){
            if(graph.indexOf(town) == -1){
                throw new NoSuchRouteException();
            }else{

                town = graph.get(graph.indexOf(town));

                if(previous !=null){
                    ArrayList<Route> list = previous.getRouteTo(town);
                    if(list == null || list.size() == 0){
                        throw new NoSuchRouteException();
                    }else{

                        //Under assumption we only have one direct route between two towns
                        distance += list.get(0).getDistance();
                    }
                }

                previous = town;
            }
        }
        return distance;

    }



    /**
     *
     * @param from
     * @param to
     * @return
     * @throws NoSuchRouteException
     * @throws InvalidRouteException
     */
    public static Route getShortestRoute(Town from, Town to) throws NoSuchRouteException, InvalidRouteException{

            ArrayList<Route> result =  getAllRoutes(from,to, graph.size()*2, false);
            Collections.sort(result, new Comparator<Route>() {
                @Override
                public int compare(Route o1, Route o2) {
                     if(o1.getDistance() == o2.getDistance())
                         return 0;

                     return o1.getDistance() < o2.getDistance() ? -1 : 1;
                }
            });

            return result.get(0);
    }



    /**
     * Get all routes from Town 'from' to Town 'to'
     * @param from initial Town
     * @param to final Town
     * @param limit limit of steps
     * @return
     * @throws NoSuchRouteException
     */
    public static ArrayList<Route> getAllRoutes(Town from, Town to, int limit, boolean distance) throws NoSuchRouteException, InvalidRouteException {

        //Validations
        int indexFrom = graph.indexOf(from);
        if(indexFrom == -1)
            throw new NoSuchRouteException();

        int indexTo = graph.indexOf(to);
        if(indexTo == -1)
            throw new NoSuchRouteException();


        //Begin of search algorithm
        Town initial = graph.get(indexFrom);
        Town finalt = graph.get(indexTo);
        ArrayList<Town> visited =  new ArrayList<>();
        LinkedList<Route> list = new LinkedList<>();


        ArrayList<Route> rtmp = initial.getRouteTo(finalt) ;
        ArrayList<Route> results = new ArrayList<>();

        //if there is a direct path return it
        if (rtmp != null){
            results.add(rtmp.get(0));
            return results;
        }else{
            list.addAll(initial.getFromThisTown());
        }

        //While there are paths to explore...
        while(!list.isEmpty()){


            Route current = list.pop();
            ArrayList<Route> neighbours =  current.getToTown().getFromThisTown();

            for(Route adjacent : neighbours){

                Route aux = current.concatenate(adjacent);


                if(aux.getToTown().equals(finalt)){

                    results.add(aux);

                    for (Route cycle: finalt.getFromThisTown()){
                        Route aux2 = aux.concatenate(cycle);
                        if(!list.contains(aux2) && ((!distance && aux2.getSteps()  < limit) || (distance && aux2.getDistance() < limit))) {
                            list.add(aux2);
                        }
                    }
                }else{
                    if(!list.contains(aux) && ((!distance && aux.getSteps()  < limit) || (distance && aux.getDistance() < limit))) {
                        list.add(aux);
                    }

                }
            }
        }

        return results;

    }


    /**
     * Build initial graph
     * @param routes
     */
    private static void initGraph(ArrayList<Route> routes){
        for (Route route: routes) {


            int index = graph.indexOf(route.getFromTown());
            if(index == -1){
                graph.add(route.getFromTown());
                route.getFromTown().getFromThisTown().add(route);
            }else{
                route.setFromTown(graph.get(index));
                graph.get(index).getFromThisTown().add(route);
            }

            index = graph.indexOf(route.getToTown());
            if(index == -1){
                graph.add(route.getToTown());
                route.getToTown().getToThisTown().add(route);
            }else{
                route.setToTown(graph.get(index));
                graph.get(index).getToThisTown().add(route);
            }
        }
    }


    //Debugging purposes
    /*public static void printTable(){

        ArrayList<Town> alreadyPrinted = new ArrayList<>();

       for (Town k : graph){
           if(alreadyPrinted.indexOf(k) == -1){
               alreadyPrinted.add(k);
               System.out.println("Town: " + k);
           }
       }

    }*/


}

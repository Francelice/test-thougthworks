package com.thoughtworks.test;

import com.thoughtworks.trains.Trains;
import com.thoughtworks.trains.exceptions.InvalidRouteException;
import com.thoughtworks.trains.exceptions.NoSuchRouteException;
import com.thoughtworks.trains.operations.RailroadInformation;
import com.thoughtworks.trains.pojos.Route;
import com.thoughtworks.trains.pojos.Town;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by cchang on 8/5/17.
 */
public class TrainsTest {

    @Before
    public void init(){
        Trains.init(" AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7");
        //RailroadInformation.printTable();
    }

    @Test
    public void Question1() {

        //Question #1
        ArrayList<Town> test = new ArrayList<>();
        test.add(new Town("A"));
        test.add(new Town("B"));
        test.add(new Town("C"));

        int distance = 0;
        try {
            distance = RailroadInformation.routeDistance(test);
            System.out.println("Output #1: " + distance);
        } catch (NoSuchRouteException e) {
            System.out.println("Output #1: NO SUCH ROUTE ");
        }

        Assert.assertEquals(9, distance);

    }

    @Test
    public void Question2() {

        //Question #2
        ArrayList<Town> test = new ArrayList<>();
        int distance = 0;

        test.add(new Town("A"));
        test.add(new Town("D"));

        try {
            distance = RailroadInformation.routeDistance(test);
            System.out.println("Output #2: " + distance);
        } catch (NoSuchRouteException e) {
            System.out.println("Output #2: NO SUCH ROUTE ");
        }
        Assert.assertEquals(5, distance);
    }

    @Test
    public void Question3() {

        //Question #3
        ArrayList<Town> test = new ArrayList<>();
        int distance = 0;

        test.add(new Town("A"));
        test.add(new Town("D"));
        test.add(new Town("C"));

        try {
            distance = RailroadInformation.routeDistance(test);
            System.out.println("Output #3: " + distance);
        } catch (NoSuchRouteException e) {
            System.out.println("Output #3: NO SUCH ROUTE ");
        }
        Assert.assertEquals(13, distance);

    }

    @Test
    public void Question4() {

        //Question #4
        ArrayList<Town> test = new ArrayList<>();
        int distance = 0;

        test.add(new Town("A"));
        test.add(new Town("E"));
        test.add(new Town("B"));
        test.add(new Town("C"));
        test.add(new Town("D"));

        try {
            distance = RailroadInformation.routeDistance(test);
            System.out.println("Output #4: " + distance);
        } catch (NoSuchRouteException e) {
            System.out.println("Output #4: NO SUCH ROUTE ");
        }
        Assert.assertEquals(22, distance);

    }

    @Test
    public void Question5(){

        //Question #5
        ArrayList<Town> test = new ArrayList<>();
        int distance = 0;

        test.add(new Town("A"));
        test.add(new Town("E"));
        test.add(new Town("D"));

        try {
            distance = RailroadInformation.routeDistance(test);
            System.out.println("Output #5: "+ distance);
        } catch (NoSuchRouteException e) {
            System.out.println("Output #5: NO SUCH ROUTE ");
            distance = 0;
        }
        Assert.assertEquals(0,distance);

    }

    @Test
    public void Question6(){

        ArrayList<Route> res = new ArrayList<>();

        try {
            res = RailroadInformation.getAllRoutes(new Town("C"),new Town("C"),3, false);
            System.out.println("Output #6: " + res.size());
        } catch (NoSuchRouteException | InvalidRouteException e1) {
            System.out.println("Output #6: NO SUCH ROUTE ");
        }

        Assert.assertEquals(2,res.size());

    }

    @Test
    public void Question7(){

        List<Route> res;

        try {
            res = RailroadInformation.getAllRoutes(new Town("A"),new Town("C"),4, false)
                    .stream()
                    .filter((k)-> {return k.getSteps() == 4;})
                    .collect(Collectors.toList());

            System.out.println("Output #7: " + res.size());

        } catch (NoSuchRouteException | InvalidRouteException e1) {
            res = new ArrayList<>();
            System.out.println("Output #7: NO SUCH ROUTE ");
        }

        Assert.assertEquals(3,res.size());

    }

    @Test
    public void Question8(){

        Route res;

        try {
            res = RailroadInformation.getShortestRoute(new Town("A"),new Town("C"));
            System.out.println("Output #8: " + res.getDistance());

        } catch (NoSuchRouteException | InvalidRouteException e1) {
            res = new Route();
            System.out.println("Output #8: NO SUCH ROUTE ");
        }

        Assert.assertEquals(9,res.getDistance());

    }

    @Test
    public void Question9(){

        Route res;

        try {
            res = RailroadInformation.getShortestRoute(new Town("B"), new Town("B"));
            System.out.println("Output #9: " + res.getDistance());

        } catch (NoSuchRouteException | InvalidRouteException e1) {
            res = new Route();
            System.out.println("Output #9: NO SUCH ROUTE ");
        }

        Assert.assertEquals(9,res.getDistance());

    }


    @Test
    public void Question10(){

        List<Route> res = new ArrayList<>();

        try {
            res = RailroadInformation.getAllRoutes(new Town("C"),new Town("C"), 30, true)
                    .stream()
                    .filter((k)-> {return k.getDistance() < 30;})
                    .collect(Collectors.toList());

        } catch (NoSuchRouteException | InvalidRouteException e1) {
            res = new ArrayList<>();
            System.out.println("Output #10: NO SUCH ROUTE ");
        }

        Assert.assertEquals(7,res.size());

    }
}